// console.log("HI")

function Pokemon(name, level){
	this.name = name;
	this.lvl = level;
	this.health = 2 * level;
	this.atk = 10;

	// Methods
	this.tackle = function(target){
		let result = target.health - this.atk;
		target.health = result;

		console.log(`${this.name} tackled ${this.name}`)
		console.log(`${target.name} health is now reduced to ${this.health}`);
		if(target.health === 0){
			console.log(`${this.name} defeated ${target.name}`)
		}
			this.faint = function(){
				console.log(`${this.name} fainted`)
		};
	};
};

let charmander = new Pokemon("lucario", 50);
let lucario = new Pokemon("charmander", 50);


console.log(charmander);
console.log(lucario);

charmander.tackle(lucario);
lucario.tackle(charmander);

charmander.tackle(lucario);
lucario.tackle(charmander);

charmander.tackle(lucario);
lucario.tackle(charmander);

charmander.tackle(lucario);
lucario.tackle(charmander);

charmander.tackle(lucario);
lucario.tackle(charmander);

charmander.tackle(lucario);
lucario.tackle(charmander);

charmander.tackle(lucario);
lucario.tackle(charmander);

charmander.tackle(lucario);
lucario.tackle(charmander);

charmander.tackle(lucario);
lucario.tackle(charmander);

lucario.faint(charmander);
